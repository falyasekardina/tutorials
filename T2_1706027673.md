Tutorial 1

Nama : Falya Aqiela Sekardina

NPM : 1706027673


1. Scene BlueShip dan StonePlatform sama-sama memiliki sebuah child node
bertipe Sprite. Apa fungsi dari node bertipe Sprite?
	- Tipe Sprite merupakan node yang menunjukkan 2D (gambar biasa)

	
2. Root node dari scene BlueShip dan StonePlatform menggunakan tipe yang
berbeda. BlueShip menggunakan tipe RigidBody2D, sedangkan StonePlatform
menggunakan tipe StaticBody2D. Apa perbedaan dari masing-masing tipe node?
	- RigidBody2D biasa digunakan untuk permainan 3D atau permainan yang membutuhkan simulasi fisik yang terjadi
	- StaticBody2D banyak digunakan untuk permainan 2D dan permainan yang membutuhkan kontrol dari user
	
	
3. Ubah nilai atribut Mass dan Weight pada tipe RigidBody2D secara
bebas di scene BlueShip, lalu coba jalankan scene Main. Apa yang
terjadi?
	- Perubahan mass yang dimiliki scene BlueShip, semakin menambah mass maka saat scene bergerak semakin lambat namun perubahannya tidak siginifikan
	
	
4. Ubah nilai atribut Disabled pada tipe CollisionShape2D di scene
StonePlatform, lalu coba jalankan scene Main. Apa yang terjadi?
	- Scene tersebut akan menjadi objek pasif, sehingga objek akan bergerak vertikal (jatuh)
	
	
5. Pada scene Main, coba manipulasi atribut Position, Rotation, dan Scale
milik node BlueShip secara bebas. Apa yang terjadi pada visualisasi
BlueShip di Viewport?
	- Position : Mengubah posisi objek berdasarkan derajat x dan y yang telah ditentukan
	- Rotation Degree : Mengubah derajat suatu objek
	- Scale : Mengecilkan dan membesarkan ukuran objek
	
	
6. Pada scene Main, perhatikan nilai atribut Position node PlatformBlue,
StonePlatform, dan StonePlatform2. Mengapa nilai Position node
StonePlatform dan StonePlatform2 tidak sesuai dengan posisinya di dalam
scene (menurut Inspector) namun visualisasinya berada di posisi yang tepat?
	- Dikarenakan StonePlatform dan StonePlatform2 merupakan child node dari PlatformBlue. Sehingga ketika kita meletakkan parent node pada posisi x = 300, maka child nodenya memiliki posisi x = 300 meskipun x pada masing-masing child node bernilai 0